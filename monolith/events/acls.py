import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": city + " " + state,
    }
    response = requests.get(url, headers=headers, params=params)
    content = json.loads(response.content)
    try:
        return {
            "picture_url": content["photos"][0]["src"]["original"],
        }
    except:
        return {"picture_url": None}


def get_weather(city, state):
    url_1 = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&limit=1&appid={OPEN_WEATHER_API_KEY}"
    response_1 = requests.get(url_1)
    content_1 = json.loads(response_1.content)
    lon = content_1[0]["lon"]
    lat = content_1[0]["lat"]
    url_2 = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
    response_2 = requests.get(url_2)
    content_2 = json.loads(response_2.content)
    try:
        return {
            "description": content_2["weather"][0]["description"],
            "temp": content_2["main"]["temp"],
        }
    except:
        return {"weather": None}
